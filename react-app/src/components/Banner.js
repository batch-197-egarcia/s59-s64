
import { Row, Col, Button } from 'react-bootstrap';


export default function Banner() {
    return(
        <Row> 
            <Col className="p-5">
                <h1>My E-Commerce</h1>
                <p>  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed porttitor viverra 
                quam vitae imperdiet. Pellentesque ultrices lacus vulputate dolor lobortis ullamcorper. 
                Fusce non efficitur nibh, nec vestibulum neque. Nam consectetur orci varius libero imperdiet eleifend. Morbi pharetra scelerisque cursus. 
                Quisque dignissim sed mauris nec consectetur. Phasellus imperdiet porta elit eu interdum. Suspendisse molestie dui et arcu ultrices ultricies. 
                Sed ante eros, imperdiet sed ex eget, vehicula sollicitudin magna. In et molestie eros, non fermentum mi.

</p>
               <Button variant="primary">Explore</Button>
            </Col>
        </Row>
    )
};


