// Old practice in importing components
// import Navbar from 'react-bootstrap/NavBar';
// import Nav from 'react-bootstrap/Nav';
//
//import { useState } from 'react' 
import { useContext } from 'react' 
// New practice in importing components;
import { Navbar, Nav, Container } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import UserContext from '../UserContext';


export default function AppNavbar() {
	
	//useContext hook
	const {user} = useContext(UserContext);
	//getItem retrieves the data stored in the localStorage
	// const [user, setUser] = useState(localStorage.getItem("email"))
	 console.log(user);

	return (

		<Navbar bg="light" expand="lg">
		    <Container fluid>
		      <Navbar.Brand as={Link} to='/'>E-Commerce</Navbar.Brand>
		      <Navbar.Toggle aria-controls="navbarScroll" />
		      <Navbar.Collapse id="navbarScroll">
		        <Nav className="ml-auto">
		          <Nav.Link as={Link} to='/'>Home</Nav.Link>
		        
				  
				  { (user.email !== null) ? 
				 	 	<Nav.Link as={Link} to='/logout'>Logout</Nav.Link>
					:
					<>
						<Nav.Link as={Link} to='/register'>Register</Nav.Link>
						<Nav.Link as={Link} to='/login'>Login</Nav.Link>
					</>
				  }
				

		        </Nav>
		      </Navbar.Collapse>
		    </Container>
		  </Navbar>
	)
};