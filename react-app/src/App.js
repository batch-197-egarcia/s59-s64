// import logo from './logo.svg';
import { useState } from 'react';
import { Container } from 'react-bootstrap'
import { BrowserRouter as Router } from 'react-router-dom';
import { Routes, Route } from 'react-router-dom';
//import UserContext from '../UserContext';

//components
import AppNavbar from './components/AppNavbar';

//pages

import Home from './pages/Home';
import Register from './pages/Register';
import Login from './pages/Login'
import Logout from './pages/Logout';

//stylesheet
import './App.css';
import { UserProvider } from './UserContext'


function App() {
  
//const {user} = useContext(UserContext);
//console.log(user);


  //creating Global State ({})
  //state hook for the user state that is defined here for a global scope
  //also this is initialized as an object with properties from the localStorage
  //also this will be used to store the user information and will be used for validating
  //if a user is logged in on the app or not
  const [user, setUser] = useState({email: localStorage.getItem("email")}); 
  // Function for clearing the localStorage upon logout
  const unsetUser = () => {
          localStorage.clear();
  } 

  return (

    <UserProvider value={ {user, setUser, unsetUser} }>
          <Router>
     
           <AppNavbar/>
            <Container>
              <Routes>
                  <Route path='/' element={ <Home/>} />
                  <Route path='/register' element={ <Register/>} />
                  <Route path='/login' element={ <Login/>} />
                  <Route path='/logout' element={ <Logout/>} />
             

                  
              </Routes>
            </Container>
  
        </Router>
    </UserProvider>
  
  );
}

export default App;
