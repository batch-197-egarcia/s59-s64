import { Fragment,useState,useEffect,useContext } from 'react'
import { Form, Button,Container } from 'react-bootstrap'
import { Navigate } from 'react-router-dom';
import UserContext from '../UserContext';


export default function Login() {

    //destructing of userContext they are a variable here
    //allows us to consume the UserContext
    //object and its properties to be used in validation
    const {user, setUser } = useContext(UserContext);
  
  //state hook
  const[email, setEmail] = useState(""); 
  const [password,setPassword] = useState("");
  

  
  //state to determine whether submit button is enable or not
  const [isActive, setIsActive] = useState(false);


//Check if values are successfully passed

//   console.log(`Email: ${email}`);
//   console.log(`Password: ${password}`);


//Authenticates user if existing in local storage/db 
  function loginUser(e) {
   
    e.preventDefault();

    

    // setItem stores data in localStorage
    //             ("for mongoDB", from statehook)
    localStorage.setItem("email", email);

    setUser({
        email: localStorage.getItem("email")
    })

    // clear input fields
    setEmail('');
    setPassword('');
 
    console.log(`${email} has been verified! Welcome Back!`);
    alert(`Hi! ${email} you have successfully logged in !`);
};



  //useEffect for button
  useEffect(() => {
    if (email !== '' && password !== ''){
        setIsActive(true);
    } else {
        setIsActive(false);
    }
}, [email,password]);



  
  
  
  
    return (
        (user.email !== null ) ?
		<Navigate to ="/"/>
	:
  
 
        <Fragment>
        <h1 className='text-center mt-3'>LOG IN</h1>
         <Container className="container-reg-form p-3">

            
            
            <Form className="reg-form" onSubmit={(e) => {loginUser(e)}}>

            <Form.Group className="mb-3 text-center" controlId="userEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control
                            type="email"
                            value = {email}
                            placeholder="Enter your email here"
                            onChange = {(e) => {setEmail(e.target.value)}}
                            required
                        />

                
            </Form.Group>

            <Form.Group className="mb-3 text-center" controlId="password1">
                <Form.Label>Password</Form.Label>
                <Form.Control
                            type="password"
                            value = {password}
                            placeholder="Enter your Password"
                            onChange = {(e) => {setPassword(e.target.value)}}
                            required
                        />
            </Form.Group>

            
            { isActive ?
                <Button className="button-form" variant="info" type='submit' id='submitBtn' >
                Submit
            </Button>
                :
                <Button className="button-form" variant="secondary" type='submit' id='submitBtn' disabled>
                Submit
            </Button>

            }
            
            </Form>
            </Container>
        </Fragment>
      



  )
}

