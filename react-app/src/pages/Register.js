import { Fragment,useState,useEffect,useContext } from 'react'
import { Form, Button, Container } from 'react-bootstrap'
import { Navigate } from 'react-router-dom';
import UserContext from '../UserContext';




export default function Register() {

  const {user} = useContext(UserContext);
  
  //state hook
  const[firstName, setFirstName] = useState("");
  const[lastName, setLastName] = useState("");
 //const[mobileNo, setMobileNo] = useState("");
  const[email, setEmail] = useState(""); 
  const [password1,setPassword1] = useState("");
  const [password2,setPassword2] = useState("");

  
  //state to determine whether submit button is enable or not
  const [isActive, setIsActive] = useState(false);


//Check if values are successfully passed
//   console.log(`firstName: ${firstName}`);
//   console.log(`lastName: ${lastName}`);
//   console.log(`mobileNumber: ${mobileNo}`);
//   console.log(`Email: ${email}`);
//   console.log(`Password1: ${password1}`);
//   console.log(`Password2: ${password2}`);


  function registerUser(e) {
   
    e.preventDefault();

    // clear input fields
    setFirstName('');
    setLastName('');
   // setMobileNo('');
    setEmail('');
    setPassword1('');
    setPassword2('');

    alert('Thank you for registering');
};



  //useEffect for button
  useEffect(() => {
    
    if((firstName !== '' && lastName !== '' && email !== '' && password1 !== '' && password2 !== '') && (  password1 === password2)){
        setIsActive(true);
    } else {
        setIsActive(false);
    }
}, [firstName, lastName, email, password1, password2]);


  
  
  
  
    return (
      (user.email !== null) ?
      <Navigate to="/login"/>
  
      :
    <Fragment>
     <h1 className='text-center mt-3'>REGISTER</h1>
    <Container className="container-reg-form p-3">
    

   
    <Form className="reg-form" onSubmit={(e) => {registerUser(e)}}>
{/* FirstName */}
    <Form.Group className="mb-3 text-center" controlId="firstName">
        <Form.Label>First Name </Form.Label>
        <Form.Control
					type="text"
                    value = {firstName}
					placeholder="Enter your first name here"
					onChange = {(e) => {setFirstName(e.target.value)}}
                    required
				/>

      </Form.Group>

  {/* LastName */}
     <Form.Group className="mb-3 text-center" controlId="lastName">
        <Form.Label>Last Name </Form.Label>
        <Form.Control
					type="text"
                    value = {lastName}
					placeholder="Enter your last name here"
					onChange = {(e) => {setLastName(e.target.value)}}
                    required
				/>

      </Form.Group>

     





      <Form.Group className="mb-3 text-center" controlId="userEmail">
        <Form.Label>Email address</Form.Label>
        <Form.Control
					type="email"
                    value = {email}
					placeholder="Enter your email here"
					onChange = {(e) => {setEmail(e.target.value)}}
                    required
				/>

       
      </Form.Group>

      <Form.Group className="mb-3 text-center" controlId="password1">
        <Form.Label>Password</Form.Label>
        <Form.Control
					type="password"
                    value = {password1}
					placeholder="Enter your Password"
					onChange = {(e) => {setPassword1(e.target.value)}}
                    required
				/>
      </Form.Group>

      <Form.Group className="mb-3 text-center" controlId="password2">
        <Form.Label>Verify Password</Form.Label>
        <Form.Control
					type="password"
                    value = {password2}
					placeholder="Enter your Password Again"
					onChange = {(e) => {setPassword2(e.target.value)}}
                    required
				/>
      </Form.Group>
     { isActive ?
        <Button className="button-form" variant="info" type='submit' id='submitBtn' >
        Submit
      </Button>
        :
        <Button className="button-form" variant="secondary" type='submit' id='submitBtn' disabled>
        Submit
      </Button>

    }
     
    </Form>
    </Container>
    </Fragment>
  )
}

